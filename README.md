# Codespell Docker Container

## Customize

You can adjust some settings by setting the following environment variables:
- `CODESPELL_SKIP_FILES`: Comma-separated list of files to be skipped. *(default: "\*.txt,./target,./.cargo")*
- `CODESPELL_ALLOW_WORDS`: Comma-separated list of words that should not be recognized as typos. *(default: "crate")*