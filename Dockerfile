FROM python

RUN pip install git+https://github.com/codespell-project/codespell.git --break-system-packages

# default configuration
ENV CODESPELL_SKIP_FILES="*.txt,./target,./.cargo,./.git"
ENV CODESPELL_ALLOW_WORDS="crate"

WORKDIR /project

CMD ["sh", "-c", "codespell --skip=$CODESPELL_SKIP_FILES -L $CODESPELL_ALLOW_WORDS"]
